# gtksourceview-pkgbuild

PKGBUILD syntax highlight support in GtkSourceView

## What is this

This repository provides a set of files that enables syntax highlight for
[Arch Linux](https://archlinux.org)'s
[PKGBUILD](https://wiki.archlinux.org/index.php/PKGBUILD)
when using GtkSourceView-compliant text editores, e.g.
GNOME's [Gedit](https://wiki.gnome.org/Apps/Gedit),
[gnome-text-editor](https://gitlab.gnome.org/GNOME/gnome-text-editor) and
Xfce's [mousepad](https://git.xfce.org/apps/mousepad/).

This is not part of GtkSourceView. For the official source code repository
of GtkSourceView, see https://gitlab.gnome.org/GNOME/gtksourceview

## How to install

### From AUR

In the [AUR](https://wiki.archlinux.org/index.php/Arch_User_Repository)
you will find these packages:
[mime-pkgbuild](https://aur.archlinux.org/packages/mime-pkgbuild/),
[gtksourceview3-pkgbuild](https://aur.archlinux.org/packages/gtksourceview3-pkgbuild/),
[gtksourceview4-pkgbuild](https://aur.archlinux.org/packages/gtksourceview4-pkgbuild/),
[gtksourceview5-pkgbuild](https://aur.archlinux.org/packages/gtksourceview5-pkgbuild/)

To build and install these packages, run

```shell
git clone gtksourceview-pkgbuild
cd gtksourceview-pkgbuild
makepkg -si
```

### Manually

1. Download the `pkgbuild.xml` and `pkgbuild.lang`, or clone this repository
2. Install **pkgbuild.xml** to `/usr/share/mime/packages/pkgbuild.xml`
   and make sure to have
   [shared-mime-info](https://archlinux.org/packages/shared-mime-info)
   package installed;
3. For GtkSourceView3-compliant text editor,
   install **pkgbuild.lang** to
   `/usr/share/gtksourceview-3.0/language-specs/pkgbuild.lang`
   and make sure to have
   [gtksourceview3](https://archlinux.org/packages/gtksourceview3)
   package installed
4. For GtkSourceView4-compliant text editor,
   install **pkgbuild.lang** to
   `/usr/share/gtksourceview-4/language-specs/pkgbuild.lang`
   and make sure to have
   [gtksourceview4](https://archlinux.org/packages/gtksourceview4)
   package installed
5. For GtkSourceView5-compliant text editor,
   install **pkgbuild.lang** to
   `/usr/share/gtksourceview-5/language-specs/pkgbuild.lang`
   and make sure to have
   [gtksourceview5](https://archlinux.org/packages/gtksourceview5)
   package installed

Ignore the versions you don't need. e.g. Since gnome-text-editor uses
GtkSourceView5, you can skip steps 3 and 4.

## Reporting bug and suggesting improvements

Feedbacks are welcome for sure! Feel free to report a bug by
[filing an issue](https://gitlab.com/rafaelff/gtksourceview-pkgbuild/-/issues)
or even
[proposing a merge request](https://gitlab.com/rafaelff/gtksourceview-pkgbuild/-/merge_requests).
